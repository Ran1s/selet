require 'csv'

persons = CSV.read("sanak2012.csv", headers: true)

def delete_excess_gap(str)
  str.gsub!(/[ ]{2,}/, ' ')
  return str.strip!
end

def format_name(name)

  res = {
    last_name: nil,
    first_name: nil,
    middle_name: nil
  }

  return false if name == nil

  name.gsub!(/[^а-яА-Я ]+/, '')
  name = delete_excess_gap(name)

  if name =~ /^([а-яА-Я]+ [а-яА-Я]+ [а-яА-Я]+)$/
    name_separated = name.split(' ')
    res[:last_name] = name_separated[0]
    res[:first_name] = name_separated[1]
    res[:middle_name] = name_separated[2]

  elsif name =~ /^([а-яА-Я]+ [а-яА-Я]+)$/
    name_separated = name.split(' ')
    res[:last_name] = name_separated[0]
    res[:first_name] = name_separated[1]
  end
  res
end



def format_date(date)
  date.gsub!(/[^0-9.]+/, '')

  date_separated = date.split('.')

  day = date_separated[0].to_i
  mounth = date_separated[1].to_i
  year = date_separated[2].to_i

  if date =~ /^[0-9]+.[0-9]+.[0-9]+$/
    case mounth
    when 1, 3, 5, 7, 8, 10, 12
      return date if day <= 31
    when 4, 6, 9, 11
      return date if day <= 30
    when 2
      if year.modulo(4) == 0 && day <= 29 || day <= 28
        return date
      end
    end
  end

  nil

end

def format_vk(domen)
  domen.downcase!
  #domen.gsub!(/[^0-9a-z._]+/, '')
  return nil if /[^0-9a-z._\/:]+/ === domen
  if domen =~ /([a-z0-9_.]+)/
    domen = Regexp.last_match[0]
    if domen =~ /^[0-9]+$/
      return "id#{domen}"
    elsif domen =~ /^[0-9]+[a-z]+/
      return nil
    else
      return domen
    end
  else
    nil
  end
end

def format_passport(passport)
  res = {
    passport: {
      existence: false
    },
    birth_certificate: {
      existence: false
    }
  }
  passport = delete_excess_gap(passport)
  if passport =~ /([0-9]{2})[ ]?([0-9]{2})[ ]?([0-9]{6})/
    res[:passport][:existence] = true
    res[:passport][:series] = "#{Regexp.last_match[1]}#{Regexp.last_match[2]}"
    res[:passport][:number] = Regexp.last_match[3]
  elsif passport =~ /([0-9IVX]+)[-—]?([а-яА-Яa-zA-Z]{2})[ ]?[№]?[ ]?([0-9]{6})/
    res[:birth_certificate][:existence] = true
    res[:birth_certificate][:series] = "#{Regexp.last_match[1]}-#{Regexp.last_match[2]}"
    res[:birth_certificate][:number] = "№#{Regexp.last_match[3]}"
  end
  res
end



def create_person(person_initial)
  person = { data: { }, meta: { } }

  person[:data].merge!(format_name(person_initial["name"]))

  person[:meta][:first_name] = {}
  person[:meta][:first_name][:empty] = person[:data][:first_name].nil?

  person[:meta][:middle_name] = {}
  person[:meta][:middle_name][:empty] = person[:data][:middle_name].nil?


  person[:meta][:last_name] = {}
  person[:meta][:last_name][:empty] = person[:data][:last_name].nil?

  person[:data][:birthday] = format_date(person_initial["birthday"])


  person[:meta][:birthday] = {}
  person[:meta][:birthday][:empty] = person[:data][:birthday].nil?

  person[:data][:vk] = format_vk(person_initial["vk"])

  person[:meta][:vk] = {}
  person[:meta][:vk][:empty] = person[:data][:vk].nil?

  person[:data].merge!(format_passport(person_initial["passport"]))

  person[:meta][:passport] = {}
  person[:meta][:birth_certificate] = {}

  person[:meta][:birth_certificate][:series_empty] = !person[:data][:birth_certificate][:existence]
  person[:meta][:birth_certificate][:number_empty] = !person[:data][:birth_certificate][:existence]

  person[:meta][:passport][:series_empty] = !person[:data][:passport][:existence]
  person[:meta][:passport][:number_empty] = !person[:data][:passport][:existence]
  person[:data][:passport][:issued_date] = format_date(person_initial["passport_date_issue"])
  person[:meta][:passport][:issued_date_empty] = person[:data][:passport][:issued_date].nil?
  person[:data][:passport][:issued_by] = person_initial["passport_who_issued"]
  person[:meta][:passport][:issued_by_empty] = person[:data][:passport][:issued_by].nil?

  person[:data][:mother] = {}
  person[:meta][:mother] = {}

  person[:data][:mother].merge!(format_name(person_initial["mother_name"]))

  person[:meta][:mother][:first_name] = {}
  person[:meta][:mother][:first_name][:empty] = person[:data][:mother][:first_name].nil?

  person[:meta][:mother][:middle_name] = {}
  person[:meta][:mother][:middle_name][:empty] = person[:data][:mother][:middle_name].nil?

  person[:meta][:mother][:last_name] = {}
  person[:meta][:mother][:last_name][:empty] = person[:data][:mother][:last_name].nil?

  add_value(person[:data], format_name(), person[:meta])

  person
end


pers = {
  "name" => "Абаева Лейла Рустамовна ",
  "birthday" => "31.10.1997",
  "vk" => "leykababayka",
  "passport" => "9211 266539",
  "passport_who_issued" => "Отделом УФМС России по РТ в Ново-Савиновском районе г.Казани",
  "passport_date_issue" => "29.11.2011",
  "mother_name" => "Ситникова Ольга Валерьевна "
}

p create_person(pers)
=begin
count_good = 0
count_nil = 0
persons.each do |person|
  #
  if person["passport"] == nil
    count_nil += 1
    next
  end
  #
  if format_passport(person["passport"]) != false
    count_good += 1
    #p format_passport(person["passport"])
  else
    puts "#{person["passport"]}"
  end
end

puts "all: #{persons.size}"
puts "good: #{count_good}"
puts "nil: #{count_nil}"
=end

#puts format_passport('III-КБ №514743 (повторно)')
