require 'minitest/autorun'
require './filter.rb'

class FilterTest < MiniTest::Test

  def test_valid_name
    response = format_name('Сагдеева Гульчачак Зуфаровна')
    assert_equal({last_name: 'Сагдеева', first_name: 'Гульчачак', middle_name: 'Зуфаровна'}, response)
  end

  def test_valid_name_with_wrong_symbols
    response = format_name('Сагдеева Гульчачак1 Зуфаровна')
    assert_equal({last_name: 'Сагдеева', first_name: 'Гульчачак', middle_name: 'Зуфаровна'}, response)
  end

  def test_valid_name_with_wrong_symbols2
    response = format_name('1Саг%д$еева 1Гульч%ачак1 1@Зуфар%овна!')
    assert_equal({last_name: 'Сагдеева', first_name: 'Гульчачак', middle_name: 'Зуфаровна'}, response)
  end

  def test_invalid_name_with_excess_words
    response = format_name('Сагдеева Гульчачак Зуфаровна йцу')
    assert_equal({:last_name=>nil, :first_name=>nil, :middle_name=>nil}, response)
  end

  def test_invalid_name_with_excess_words2
    response = format_name('Сагдеева Гульчачак Зуфаровна йцу')
    assert_equal({:last_name=>nil, :first_name=>nil, :middle_name=>nil}, response)
  end

  def test_valid_date
    response = format_date('21.12.2013')
    assert_equal('21.12.2013', response)
  end

  def test_valid_date_2
    response = format_date('29.4.2013')
    assert_equal('29.4.2013', response)
  end

  def test_valid_date_with_wrong_symbols
    response = format_date('>21.1&2.201*3')
    assert_equal('21.12.2013', response)
  end

  def test_invalid_date_with_excess_parts
    response = format_date('21.12.2013.123')
    assert_equal(nil, response)
  end

  def test_invalid_date_29_february
    response = format_date('29.2.2013')
    assert_equal(nil, response)
  end

  def test_invalid_date_13_mounth
    response = format_date('29.13.2013')
    assert_equal(nil, response)
  end

  def test_valid_vk
    response = format_vk('id12345678')
    assert_equal('id12345678', response)
  end

  def test_valid_vk_with_wrong_symbols
    response = format_vk('id1#2345#678%')
    assert_equal(nil, response)
  end

  def test_valid_vk_without_prefix
    response = format_vk('12345678')
    assert_equal('id12345678', response)
  end

  def test_invalid_vk_starting_with_digits
    response = format_vk('1jfds5678')
    assert_equal(nil, response)
  end

  def test_valid_vk_domen
    response = format_vk('qwerty')
    assert_equal('qwerty', response)
  end

  def test_valid_passport
    response = format_passport('9211 266539')
    assert_equal({passport: {existence: true, series: '9211', number: '266539'}, birth_certificate: {existence: false}}, response)
  end

  def test_valid_passport_2
    response = format_passport('92 11 266539')
    assert_equal({passport: {existence: true, series: '9211', number: '266539'}, birth_certificate: {existence: false}}, response)
  end

  def test_valid_passport_birth_certificate
    response = format_passport('I-КБ №480079')
    assert_equal({passport: {existence: false}, birth_certificate: {existence: true, series: 'I-КБ', number: '№480079'}}, response)
  end

  def test_valid_passport_birth_certificate2
    response = format_passport('III-КБ №514743 (повторно)')
    assert_equal({passport: {existence: false}, birth_certificate: {existence: true, series: 'III-КБ', number: '№514743'}}, response)
  end


end
